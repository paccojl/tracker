package com.example.tracker.entity;

import java.util.Set;
import java.util.TreeSet;

public class User {

    Long id;
    String name;
    Set<Long> counters = new TreeSet<>();

    public User(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<Long> getCounters() {
        return counters;
    }
}
