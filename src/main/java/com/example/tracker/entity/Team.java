package com.example.tracker.entity;

import java.util.Set;
import java.util.TreeSet;

public class Team {

    private Long id;
    private String name;
    private Set<Long> users = new TreeSet<>();

    public Team(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<Long> getUsers() {
        return users;
    }
}
