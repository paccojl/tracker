package com.example.tracker.dto;

import java.util.Map;

public class UserDetailsDto {
    private Long id;
    private String name;
    private Map<Long,Long> counters;

    public UserDetailsDto(Long id, String name, Map<Long, Long> counters) {
        this.id = id;
        this.name = name;
        this.counters = counters;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Map<Long, Long> getCounters() {
        return counters;
    }

}
