package com.example.tracker.dto;

import java.util.Map;

public class TeamListDto {
    Map<Long, TeamDto> teams;

    public TeamListDto(Map<Long, TeamDto> teams) {
        this.teams = teams;
    }

    public Map<Long, TeamDto> getTeams() {
        return teams;
    }
}
