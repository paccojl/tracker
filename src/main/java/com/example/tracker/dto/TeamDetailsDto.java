package com.example.tracker.dto;

import java.util.Map;
import com.example.tracker.entity.Team;

public class TeamDetailsDto {
    private Long id;
    private String name;
    private Map<Long, UserDto> users;

    public TeamDetailsDto(Long id, String name, Map<Long, UserDto> users) {
        this.id = id;
        this.name = name;
        this.users = users;
    }

    public TeamDetailsDto(Team team, Map<Long, UserDto> userDtos) {
         new TeamDetailsDto(team.getId(), team.getName(), userDtos);
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Map<Long, UserDto> getUsers() {
        return users;
    }

}
