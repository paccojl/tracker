package com.example.tracker.dto;

import java.util.Map;

public class TeamDto {

    private final String name;
    private final Long counterSum;

    public TeamDto(String name, Long counterSum) {
        this.name = name;
        this.counterSum = counterSum;
    }

    public String getName() {
        return name;
    }

    public Long getCounterSum() {
        return counterSum;
    }
}
