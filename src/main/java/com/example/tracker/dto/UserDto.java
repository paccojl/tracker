package com.example.tracker.dto;

public class UserDto {
    private final String name;
    private final Long sum;

    public UserDto(String name, Long sum) {
        this.name = name;
        this.sum = sum;
    }

    public String getName() {
        return name;
    }

    public Long getSum() {
        return sum;
    }
}
