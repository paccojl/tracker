package com.example.tracker;

import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.example.tracker.dto.TeamDetailsDto;
import com.example.tracker.dto.TeamListDto;
import com.example.tracker.dto.UserDetailsDto;
import com.example.tracker.dto.UserDto;
import com.example.tracker.entity.Team;
import com.example.tracker.entity.User;
import com.example.tracker.service.CounterService;
import com.example.tracker.service.TeamService;
import com.example.tracker.service.UserService;

@RestController
public class TrackerController {

    TeamService teamService;
    UserService userService;
    CounterService counterService;

    public TrackerController(TeamService teamService, UserService userService, CounterService counterService) {
        this.teamService = teamService;
        this.userService = userService;
        this.counterService = counterService;
    }

    @GetMapping("team/{teamId}")
    public ResponseEntity<TeamDetailsDto> getTeamDetails(@PathVariable Long teamId){
        Team team = teamService.getTeam(teamId);

        Map<Long, UserDto> userMap = team.getUsers().stream().collect(Collectors.toMap(userId -> userId, userId -> {
            User user = userService.getUser(userId);
            return new UserDto(user.getName(), counterService.getCountersAccumulated(user.getCounters()));
        }));
        return ResponseEntity.ok(new TeamDetailsDto(team, userMap));
    }

    @GetMapping("user/{userId}")
    public ResponseEntity<UserDetailsDto> getUserDetails(@PathVariable Long userId){
        User user = userService.getUser(userId);
        return ResponseEntity.ok(new UserDetailsDto(user.getId(),user.getName(),counterService.getCounters(user.getCounters())));
    }

    @GetMapping("teams")
    public ResponseEntity<TeamListDto> getTeams(){
        //TODO
        return new ResponseEntity<>()
    }




}
