package com.example.tracker.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class CounterService {

    Map<Long,Long> counterMap = new HashMap<>();

    Long idSequencer = 0L;

    public Long createCounter(){
        Long counterId = idSequencer++;
        counterMap.put(counterId,0L);
        return counterId;
    }

    public void deleteCounter(Long counterId){
        counterMap.remove(counterId);
    }

    public Long incrementCounter(Long counterId, Long amount){
        Long counterVal = counterMap.get(counterId);
        counterMap.put(counterId, counterVal + amount);
        return counterVal + amount;
    }

    public void setCounter(Long counterId, Long val){
        counterMap.put(counterId, val);
    }

    public Map<Long,Long> getCounters(Set<Long> counters){
        return counters.stream().collect(Collectors.toMap(counterId -> counterId, counterId -> counterMap.get(counterId)));
    }

    public Long getCountersAccumulated(Set<Long> counters){
        return counters.stream().map(counterMap::get).reduce(Long::sum).orElse(0L);
    }
}
