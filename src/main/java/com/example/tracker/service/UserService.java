package com.example.tracker.service;


import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.example.tracker.entity.User;

@Service
public class UserService {

    public UserService(CounterService counterService) {
        this.counterService = counterService;
    }

    private final CounterService counterService;

    private final Map<Long, User> userStorage = new HashMap<>();
    private Long idSequencer = 0L;

    public User getUser(Long userId){
        return userStorage.get(userId);
    }

    public User addUser(String name){
        Long id = idSequencer++;
        User user = new User(id, name);
        userStorage.put(id, user);
        return user;
    }

    public User addCounter(Long userId){
        Long counterId = counterService.createCounter();
        User user = getUser(userId);
        user.getCounters().add(counterId);
        return user;
    }

    public User deleteCounter(Long userId, Long counterId){
        User user = getUser(userId);
        counterService.deleteCounter(counterId);
        user.getCounters().remove(counterId);
        return user;
    }

    public void deleteUser(Long userId){
        User user = getUser(userId);
        for (Long counter : user.getCounters()) {
            counterService.deleteCounter(counter);
        }
        userStorage.remove(userId);
    }

}
