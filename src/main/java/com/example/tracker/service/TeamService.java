package com.example.tracker.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.springframework.stereotype.Service;
import com.example.tracker.entity.Team;

@Service
public class TeamService {

    private final Map<Long, Team> teamStorage = new HashMap<>();
    private Long idSequence = 0L;

    public Team addTeam(String name){
        Long id = idSequence++;
        Team team = new Team(id, name);
        teamStorage.put(id,team);
        return team;
    }

    public Team getTeam(Long teamId){
        return teamStorage.get(teamId);
    }

    public Team addUsers(Long teamId, Set<Long> userIds){
        Team team = getTeam(teamId);
        team.getUsers().addAll(userIds);
        return team;
    }

    public Team removeUsers(Long teamId, Set<Long> userIds){
        Team team = getTeam(teamId);
        team.getUsers().removeAll(userIds);
        return team;
    }

    public void removeTeam(Long teamId){
        teamStorage.remove(teamId);
    }

    public Collection<Team> getTeams(){
        return teamStorage.values();
    }

}
